﻿$(document).ready(function () {

    //agregar factura
    $('.btn_addFactura').on('click', function () {
        $('#factura_add').modal('show');
    });
    // funcion para alertar de campos vacios
    function emptyRecord(record) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Faltan campos debe especificar ' + record + '',
            confirmButtonText: 'Aceptar'
        })
        return false;
    }

    //agregar usuario
    $('#agregarUsuario').on('click', function () {
        var Rut = $('#rut').val();
        var Nombre_usr = $('#nombre').val();
        var Apellido_p = $('#apellidop').val();
        var Apellido_m = $('#apellidom').val();
        var Contrasena = $('#password').val();
        var Id_tipo_usuario = $('#tipousuario').val();
        console.log(Rut);
        $.ajax({
            type: "POST",
            url: "/Home/insertUsuario",
            data: { Rut:Rut, Nombre_usr: Nombre_usr, Apellido_p: Apellido_p, Apellido_m: Apellido_m, Contrasena: Contrasena, Id_tipo_usuario: Id_tipo_usuario },

            success: function (data) {
                Swal.fire({
                    title: 'Usuario Agregado!',
                    text: 'Usuario agregado con exito',
                    icon: 'success',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        window.location.reload();
                    }
                })
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }

        });
        return false;
    });


    // eliminar usuario
    $('#userRecords').on('click', '.deleteUser',function () {
        var Rut = $(this).data('rut');
        $.ajax({
            type: "POST",
            url: "/Home/deleteUsuario",
            data: { Rut: Rut},

            success: function (data) {
                Swal.fire({
                    title: 'Usuario Eliminado!',
                    text: 'Usuario eliminado con exito',
                    icon: 'success',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        window.location.reload();
                    }
                })
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }

        });
        return false;
    });

    // editar usuario
    $('#userRecords').on('click', '.editUser', function () {
        var Rut = $(this).data('rut');
        var Nombre_usr = $(this).data('nombre');
        var Apellido_p = $(this).data('apellidop');
        var Apellido_m = $(this).data('apellidom');
        var Contrasena = $(this).data('contrasena');
        $('#edit_user').modal({ backdrop: 'static', keyboard: false, show: true });
        $('[name="editrut"]').val(Rut);
        $('[name="editnombre"]').val(Nombre_usr);
        $('[name="editapellidop"]').val(Apellido_p);
        $('[name="editapellidom"]').val(Apellido_m);
        $('[name="editpassword"]').val(Contrasena);
    });

    $('#btn_updateuser').on('click', function () {

        var Rut = $('#editrut').val(); ;
        var Nombre_usr = $('#editnombre').val(); ;
        var Apellido_p = $('#editapellidop').val(); ;
        var Apellido_m = $('#editapellidom').val(); ;
        var Contrasena = $('#editpassword').val(); ;
        var Id_tipo_usuario = $('#edittipousuario').val();;

        if (Nombre_usr == null || Nombre_usr == '') {
            emptyRecord('Nombre de usuario');
            
        }
        else if (Apellido_p == null || Apellido_p == "" ) {
            emptyRecord('Apellido Paterno');
        }
        else if (Apellido_m == null || Apellido_m == "") {
            emptyRecord("Apellido Materno");
        }
        else if (Contrasena == null || Contrasena == "") {
            emptyRecord("Contraseña");
        }
        else {
            $.ajax({
                type: "POST",
                url: '/Home/editUsuario',
                data: { Rut: Rut, Nombre_usr: Nombre_usr, Apellido_p: Apellido_p, Apellido_m: Apellido_m, Contrasena: Contrasena, Id_tipo_usuario: Id_tipo_usuario },

                success: function (data) {

                    $('#edit_user').modal('hide');
                    Swal.fire({
                        title: 'Usuario Actualizado!',
                        text: 'Usuario actualizado con exito',
                        icon: 'success',
                        confirmButtonText: 'Aceptar'
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            window.location.reload();
                        }
                    })
                }
            });
            return false;
        }

      
    });

  


    $('#productRecords').on('click', '.editCantidad', function () {
        var idproducto = $(this).data('idproducto');
        var nombreproducto = $(this).data('nombreproducto');
        var cantidadproducto = $(this).data('cantidadproducto');
        $('#edit_cantidad').modal({ backdrop: 'static', keyboard: false, show: true });
        $('[name="editidproducto"]').val(idproducto);
        $('[name="editnombreproducto"]').val(nombreproducto);
        $('[name="editcantidadproducto"]').val(cantidadproducto);
    });


    $('#btn_updateproduct').on('click', function () {
        var idProducto = $('#editidproducto').val();
        var cantidad = $('#editcantidadproducto').val();

        if (cantidad < 0) {
            emptyRecord('Cantidad no debe ser  menor a 0');

        } else {
            $.ajax({
                type: "POST",
                url: "/Home/UpdateRetirarProducto",
                data: { idProducto: idProducto, cantidad: cantidad },

                success: function (data) {
                    Swal.fire({
                        title: 'Producto Actualizado!',
                        text: 'Cantidad de producto actualizada  con exito',
                        icon: 'success',
                        confirmButtonText: 'Aceptar'
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            window.location.reload();
                        }
                    })
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                }

            });
            return false;
        }
    });

    // funcion para agregar nuevos productos
    $('#add_product').on('click', function () {
       
        var nombreProducto = $('#nombreproducto').val();
        var cantidadProducto = $('#cantidadproducto').val();
        var categoria = $('#categoria').val();
        console.log(categoria)
        if (nombreProducto == null || nombreProducto == '') {
            emptyRecord('Nombre del producto no puede estar vacio');
        } else if (cantidadProducto < 0) {
            emptyRecord('Cantidad de producto no debe ser menor a 0');
        }
        else {
            $.ajax({
                type: "POST",
                url: "/Home/InsertarProducto",
                data: { nombreProducto: nombreProducto, cantidad: cantidadProducto, categoria: categoria },

                success: function (data) {
                    Swal.fire({
                        title: 'Producto Agregado!',
                        text: 'Producto agregado  con exito',
                        icon: 'success',
                        confirmButtonText: 'Aceptar'
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            window.location.reload();
                        }
                    })
                },
                error: function (xhr, textStatus, error) {
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                }

            });
            return false;
        }



    });
   
});