﻿using DashboardInventory.Models;
using javax.jws;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;


namespace DashboardInventory.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        List<Usuario> listUsuarios = new List<Usuario>();
        List<Factura> listFacturas = new List<Factura>();
        List<Producto> listProductos = new List<Producto>();
        List<Categoria> listCategorias = new List<Categoria>();
        static Usuario user = new Usuario();
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(string rut , string password) // recibe rut y password desde el login 
        {
            
            using (MySqlConnection _conn = new MySqlConnection(@"Server=localhost;port=3306;Database=inventario;UID=root;SslMode=none;password=gamp2297;"))
            {
                _conn.Open();
                var query = new MySqlCommand("SELECT * FROM usuario u  left join tipo_usuario tu  on u.Id_tipo_usuario = tu.Id_tipo_usuario", _conn);
                MySqlDataReader reader = query.ExecuteReader();
                while (reader.Read())
                {
                    Usuario us = new Usuario();
                    us.rut = (string)reader["Rut"];
                    us.nombre = (string)reader["Nombre_usr"];
                    us.apellidoP = (string)reader["Apellido_p"];
                    us.apellidoM = (string)reader["Apellido_m"];
                    us.password = (string)reader["Contrasena"];
                    us.tipoUsuario = (string)reader["Tipo_usuario"];
                    listUsuarios.Add(us);
                }
                _conn.Close();
            }
            if (rut != null && password != null)
            {
               
                foreach (Usuario u in listUsuarios)
                {
                    if (u.rut == rut && u.password == password)
                    {
                        user = u;
                        ViewData["nombre"] = user.nombre;
                        ViewData["apellidoP"] = user.apellidoP;
                        return View("Home");
                    }
                }
            }else
            {
                return Ok("vacio");
            }
            
            return View();
        }
            
        public IActionResult Factura()
        {
            
            
            using (MySqlConnection _conn = new MySqlConnection(@"Server=localhost;port=3306;Database=inventario;UID=root;SslMode=none;password=gamp2297;"))
            {
                _conn.Open();
                var query = new MySqlCommand("SELECT * FROM factura f  left join relacion_fc fd  on fd.Id_factura=f.Id_factura left join distribuidor d on d.Rut_emp = fd.Rut_emp ", _conn);
                MySqlDataReader reader = query.ExecuteReader();

                while (reader.Read())
                {
                    Factura f = new Factura();
                    f.idFactura = Convert.ToInt16(reader["Id_factura"]);
                    f.numFolio = Convert.ToInt32(reader["Num_folio"]);
                    f.precioTotal = Convert.ToDouble(reader["Precio_total"]);
                    f.fechaIngreso = Convert.ToDateTime(reader["Fecha_ingreso"]);
                    f.nombreEmpresa = Convert.ToString(reader["Nombre_emp"]);
                    listFacturas.Add(f);
                }
               
                _conn.Close();
            }
            ViewData["nombre"] = user.nombre;
            ViewData["apellidoP"] = user.apellidoP;
            ViewBag.Message = listFacturas;
            return View();
        }

        public IActionResult FormFactura()
        {
            ViewData["nombre"] = user.nombre;
            ViewData["apellidoP"] = user.apellidoP;
            return View();
        }

        public IActionResult RetirarProducto()
        {
            using (MySqlConnection _conn = new MySqlConnection(@"Server=localhost;port=3306;Database=inventario;UID=root;SslMode=none;password=gamp2297;"))
            {
                _conn.Open();
                var query = new MySqlCommand("SELECT * FROM producto p  left join categoria ca  on p.Id_categoria = ca.Id_categoria", _conn);
                MySqlDataReader reader = query.ExecuteReader();
                while (reader.Read())
                {
                    Producto p = new Producto();
                    p.idProducto = Convert.ToInt16(reader["Id_producto"]);
                    p.nombreProducto = (string)reader["Nombre_producto"];
                    p.idCategoria = Convert.ToInt16(reader["Id_categoria"]);
                    p.nombreCategoria = (string)reader["Categoria"];
                    p.cantidad = Convert.ToDouble(reader["cantidad"]);
                    listProductos.Add(p);
                }
                _conn.Close();

            }


            ViewData["nombre"] = user.nombre;
            ViewData["apellidoP"] = user.apellidoP;
            ViewBag.Message = listProductos;
            return View();
        }

        [WebMethod]
        public OkObjectResult UpdateRetirarProducto(string idproducto , string cantidad)
        {
            int Id_producto = Convert.ToInt16(idproducto);
            double ccantidad = Convert.ToDouble(cantidad);
            using (MySqlConnection _conn = new MySqlConnection(@"Server=localhost;port=3306;Database=inventario;UID=root;SslMode=none;password=gamp2297;"))
            {
                _conn.Open();
                string updateCantidad = "UPDATE producto SET cantidad = @cantidad " +
                    "WHERE Id_producto = @idproducto";

                var cmd = new MySqlCommand(updateCantidad, _conn);
                cmd.Parameters.AddWithValue("@cantidad", ccantidad);
                cmd.Parameters.AddWithValue("@idproducto", Id_producto);
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
            return Ok("success");
        }

        [WebMethod]
        public OkObjectResult InsertarProducto(string nombreProducto, string cantidad, int categoria)
        {
            
            double ccantidad = Convert.ToDouble(cantidad);
            Int16 cCategoria = Convert.ToInt16(categoria);
            using (MySqlConnection _conn = new MySqlConnection(@"Server=localhost;port=3306;Database=inventario;UID=root;SslMode=none;password=gamp2297;"))
            {
                _conn.Open();
                string insertProducto = "INSERT producto (Nombre_producto, Id_categoria, cantidad) values (" +
                    "@nombreProducto," +
                    "@idCategoria," +
                    "@cantidad" +
                    ")";
                var cmdInsert = new MySqlCommand(insertProducto, _conn);
                cmdInsert.Parameters.AddWithValue("@nombreProducto", nombreProducto);
                cmdInsert.Parameters.AddWithValue("@idCategoria", cCategoria);
                cmdInsert.Parameters.AddWithValue("@cantidad", ccantidad);
                cmdInsert.ExecuteNonQuery();
                _conn.Close();

            }
            return Ok("success");
        }
        public IActionResult Usuarios()

        {

            using (MySqlConnection _conn = new MySqlConnection(@"Server=localhost;port=3306;Database=inventario;UID=root;SslMode=none;password=gamp2297;"))
            {
                _conn.Open();
                var query = new MySqlCommand("SELECT * FROM usuario u  left join tipo_usuario tu  on u.Id_tipo_usuario = tu.Id_tipo_usuario", _conn);
                MySqlDataReader reader = query.ExecuteReader();
                while (reader.Read())
                {
                    Usuario us = new Usuario();
                    us.rut = (string)reader["Rut"];
                    us.nombre = (string)reader["Nombre_usr"];
                    us.apellidoP = (string)reader["Apellido_p"];
                    us.apellidoM = (string)reader["Apellido_m"];
                    us.password = (string)reader["Contrasena"];
                    us.tipoUsuario = (string)reader["Tipo_usuario"];
                    listUsuarios.Add(us);
                }
                _conn.Close();
            }

            ViewData["nombre"] = user.nombre;
            ViewData["apellidoP"] = user.apellidoP;
            ViewBag.Message = listUsuarios;
            return View();
        }

        [WebMethod]
        public OkObjectResult  insertUsuario (string Rut, string Nombre_usr, string Apellido_p, string Apellido_m, string Contrasena, int Id_tipo_usuario)
        {
          
                using (MySqlConnection _conn = new MySqlConnection(@"Server=localhost;port=3306;Database=inventario;UID=root;SslMode=none;password=gamp2297;"))
                {
                    _conn.Open();
                    string insertUsuario = "insert usuario (Rut, Nombre_usr, Apellido_p, Apellido_m, Contrasena, Id_tipo_usuario) values ("+
                        "@Rut,"+
                        "@Nombre_usr,"+
                        "@Apellido_p,"+
                        "@Apellido_m,"+
                        "@Contrasena,"+
                        "@Id_tipo_usuario"+
                        ")";
                    var cmdUpdate = new MySqlCommand(insertUsuario, _conn);

                    cmdUpdate.Parameters.AddWithValue("@Rut", Rut);
                    cmdUpdate.Parameters.AddWithValue("@Nombre_usr", Nombre_usr);
                    cmdUpdate.Parameters.AddWithValue("@Apellido_p", Apellido_p);
                    cmdUpdate.Parameters.AddWithValue("@Apellido_m", Apellido_m);
                    cmdUpdate.Parameters.AddWithValue("@Contrasena", Contrasena);
                    cmdUpdate.Parameters.AddWithValue("@Id_tipo_usuario", Id_tipo_usuario);

                    cmdUpdate.ExecuteNonQuery();
                    _conn.Close();
                }
                return Ok("success") ;
            



        }

        public OkObjectResult deleteUsuario(string Rut)
        {

            using (MySqlConnection _conn = new MySqlConnection(@"Server=localhost;port=3306;Database=inventario;UID=root;SslMode=none;password=gamp2297;"))
            {
                _conn.Open();

                string deleteUsuario = "delete from usuario where Rut =@Rut";

                var cmd = new MySqlCommand(deleteUsuario, _conn);

                cmd.Parameters.AddWithValue("@Rut", Rut);

                cmd.ExecuteNonQuery();

                _conn.Close();
            }
            return Ok("success");

        }

        public OkObjectResult editUsuario(string Rut, string Nombre_usr, string Apellido_p, string Apellido_m, string Contrasena, int Id_tipo_usuario)
        {
            using (MySqlConnection _conn = new MySqlConnection(@"Server=localhost;port=3306;Database=inventario;UID=root;SslMode=none;password=gamp2297;"))
            {
                _conn.Open();

                string updateUsuario = "UPDATE usuario SET Nombre_usr = @nombre," +
                    "Apellido_p = @apellidoP," +
                    "Apellido_m = @apellidoM," +
                    "Contrasena = @contrasena," +
                    "Id_tipo_usuario = @idtipo WHERE Rut = @rut";

                var cmd = new MySqlCommand(updateUsuario, _conn);
                cmd.Parameters.AddWithValue("@nombre", Nombre_usr);
                cmd.Parameters.AddWithValue("@apellidoP", Apellido_p);
                cmd.Parameters.AddWithValue("@apellidoM", Apellido_m);
                cmd.Parameters.AddWithValue("@contrasena", Contrasena);
                cmd.Parameters.AddWithValue("@idtipo", Id_tipo_usuario);
                cmd.Parameters.AddWithValue("@rut", Rut);
                cmd.ExecuteNonQuery();
                _conn.Close();
            }
            return Ok("success");
        }

        public IActionResult Distribuidores()
        {
            ViewData["nombre"] = user.nombre;
            ViewData["apellidoP"] = user.apellidoP;
            return View();
        }

        public IActionResult AgregarProducto()
        {
            using (MySqlConnection _conn = new MySqlConnection(@"Server=localhost;port=3306;Database=inventario;UID=root;SslMode=none;password=gamp2297;"))
            {
                _conn.Open();
                var query = new MySqlCommand("SELECT * FROM producto p  left join categoria ca  on p.Id_categoria = ca.Id_categoria", _conn);
                MySqlDataReader reader = query.ExecuteReader();
                while (reader.Read())
                {
                    Producto p = new Producto();
                    p.idProducto = Convert.ToInt16(reader["Id_producto"]);
                    p.nombreProducto = (string)reader["Nombre_producto"];
                    p.idCategoria = Convert.ToInt16(reader["Id_categoria"]);
                    p.nombreCategoria = (string)reader["Categoria"];
                    p.cantidad = Convert.ToDouble(reader["cantidad"]);
                    listProductos.Add(p);
                }
                _conn.Close();

                _conn.Open();
                var query2 = new MySqlCommand("SELECT Id_categoria, Categoria FROM categoria", _conn);
                MySqlDataReader reader1 = query2.ExecuteReader();
                while (reader1.Read())
                {
                    Categoria c = new Categoria();
                    c.idCategoria = Convert.ToInt16(reader1["Id_categoria"]);
                    c.nombreCategoria = (string)reader1["Categoria"];
                    listCategorias.Add(c);

                }
                _conn.Close();

                ViewData["nombre"] = user.nombre;
                ViewData["apellidoP"] = user.apellidoP;
                ViewBag.Message = listProductos;
                ViewBag.Message1 = listCategorias;
                return View();
            }
            
        }

        public IActionResult Privacy()
        {
            ViewData["nombre"] = user.nombre;
            ViewData["apellidoP"] = user.apellidoP;
            return View();
        }

        public IActionResult Home()
        {
            ViewData["nombre"] = user.nombre;
            ViewData["apellidoP"] = user.apellidoP;
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
