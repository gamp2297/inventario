﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashboardInventory.Models
{
    public class Producto
    {
        public Int16 idProducto { get; set; }
        
        public string nombreProducto { get; set; }

        public Int16 idCategoria { get; set; }

        public string nombreCategoria { get; set; }
        public double cantidad { get; set; }


    }
}
