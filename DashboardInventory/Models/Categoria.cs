﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashboardInventory.Models
{
    public class Categoria
    {
        public Int16 idCategoria { get; set; }
        public string nombreCategoria { get; set; }
    }
}
