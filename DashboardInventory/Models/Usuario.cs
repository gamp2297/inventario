﻿

using System;

namespace DashboardInventory.Models
{
    public class Usuario
    {
        public string rut { get; set; }
        public string nombre { get; set; }
        public string apellidoP { get; set; }
        public string apellidoM { get; set; }
        public string password { get; set; }
        public string tipoUsuario { get; set; }
    }
}
