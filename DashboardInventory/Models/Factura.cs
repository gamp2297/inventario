﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashboardInventory.Models
{
    public class Factura 
    {
        public Int16 idFactura { get; set; }
        public Int32 numFolio { get; set; }
        public String nombreEmpresa { get; set; }
        public DateTime fechaIngreso { get; set; }
        public double precioTotal { get; set; }


    }
}
